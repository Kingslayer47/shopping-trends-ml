import pandas as pd
import numpy as np
import sklearn
from sklearn.utils import shuffle
import pickle
import matplotlib.pyplot as plt
from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.neighbors import KNeighborsClassifier 
from sklearn.model_selection import train_test_split

data = pd.read_csv('./dataset.csv')

data = data[["Age", "Gender", "Item", "Category", "Money", "Season","Size","Frequency"]]


def cleanDataset(data):
    classifications = {"Age":{"1-12":1, "13-17":2, "18-25":3,"26-40":4,"41-60":5,"60+":6}, 
                       "Money":{"0-20":1, "21-40":2, "41-60":3, "61-80":4, "81-100":5, "100+":6}
                    }
    dataArray = {"Age":[],
                 "Money":[]}
    for labels in data:
        if labels not in ["Age","Money"]:
            temp = {}
            dataArray[labels] = []
            for entry in data[labels]:
                if entry not in temp:
                    temp[entry] = len(temp)+1
                dataArray[labels].append(entry)
            classifications[labels] = temp
        else:
            for entry in data[labels]:
                dataArray[labels].append(entry)

    newDataArray = {}

    for labels in dataArray:
        newDataArray[labels] = []
        if labels == "Age":
            for data in dataArray[labels]:
                data = int(data)
                if data >= 1 and data <= 12 :
                    newDataArray[labels].append(classifications[labels]["1-12"])
                elif data >=13  and data <= 17 :
                    newDataArray[labels].append(classifications[labels]["13-17"])
                elif data >= 18 and data <= 25 :
                    newDataArray[labels].append(classifications[labels]["18-25"])
                elif data >= 26 and data <= 40 :
                    newDataArray[labels].append(classifications[labels]["26-40"])
                elif data >= 41 and data <= 60 :
                    newDataArray[labels].append(classifications[labels]["41-60"])
                else:
                    newDataArray[labels].append(classifications[labels]["60+"])
        elif labels == "Money":
            for data in dataArray["Money"]:
                data = int(data)
                if data >= 0 and data <= 20:
                    newDataArray[labels].append(classifications[labels]["0-20"])
                elif data >= 21 and data <= 40 :
                    newDataArray[labels].append(classifications[labels]["21-40"])
                elif data >= 41 and data <= 60 :
                    newDataArray[labels].append(classifications[labels]["41-60"])
                elif data >= 61 and data <= 80 :
                    newDataArray[labels].append(classifications[labels]["61-80"])
                elif data >= 81 and data <= 100 :
                    newDataArray[labels].append(classifications[labels]["81-100"])
                else:
                    newDataArray[labels].append(classifications[labels]["100+"])
        else:
            for data in dataArray[labels]:
                newDataArray[labels].append(classifications[labels][data])

    return [newDataArray,classifications]

dataSet = cleanDataset(data)

classifications = dataSet[1]

dataFrame = pd.DataFrame(dataSet[0])

dataFrame = dataFrame.sample(frac=1)

classifications = dataSet[1]
