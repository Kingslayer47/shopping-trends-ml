from main import *

class predictionInstance():

    def __init__(self, inputs=[]):
        self.dtree = DecisionTreeClassifier()
        self.clf = RandomForestClassifier()
        self.knn = KNeighborsClassifier()
        self.classifications = classifications 
        self.inputs = inputs
        self.xtr = None
        self.ytr = None
        self.xts = None
        self.yts = None

    def assignInput(self):
        assign = {}
        for keys in self.classifications:
            assign[len(assign)] = keys
        
        string = '\n'.join(str(x) + ' : ' + assign.get(x) for x in assign)

        while True:
            try:
                int_ = int(input("Select a number for the label you want to predict : \n"+string +"\n"))
                if int_ >= 0 and int_ <= len(assign)-1:
                    label = assign.get(int_)
                    break
                else:
                    print("Please input a valid number")
            except:
                pass

        
        X = np.array(dataFrame.drop([label], axis=1)) # Features
        y = np.array(dataFrame[label]) # labels


        self.xtr, self.xts, self.ytr, self.yts = train_test_split(X, y, test_size = 0.2)

        self.predictDict = self.classifications.get(assign[int_])

        def swap_key_value(dict1):
            dict2 = {}
            for (key, value) in dict1.items():
                dict2[value] = key
            return dict2

        self.predictDict = swap_key_value(self.predictDict)

        del assign[int_]
        while True:
            try:
                nPredictions = int(input("Number of predictions you'd like to make."))
                break
            except:
                print("Enter a valid Integer")

        self.predictionValue = {}
        for i in range(nPredictions):
            inputValue = []
            for key in assign:
                print(f"Pick a number for {assign.get(key)}")
                string = '\n'.join(str(self.classifications.get(assign.get(key)).get(str(x))) + ' : ' + str(x) for x in self.classifications.get(assign.get(key)))
                inpVal = int(input(string+'\n:'))
                inputValue.append(inpVal)
            self.predictionValue[i] = inputValue

        print(f"Prediction according to Decision Tree : " + self.dtreePredict())
        print(f"Prediction according to Random Forest Classifier : " + self.randomForest())
        print(f"Prediction according to K-nearest : " + self.knc())



    def dtreePredict(self):
        dtree = self.dtree.fit(self.xtr,self.ytr)
        for key in self.predictionValue:
            ans = dtree.predict([self.predictionValue.get(key)])
        
            print(f"\nPrediciton for {key} : {self.predictDict.get(int(ans[0]))}")        
        
        return self.predictDict.get(int(ans[0]))


    def randomForest(self):
        clf = self.clf.fit(self.xtr,self.ytr)
        for key in self.predictionValue:
            ans = clf.predict([self.predictionValue.get(key)])
            print(ans)
            print(f"\nPrediciton for {key} : {self.predictDict.get(int(ans[0]))}")      
        
        return self.predictDict.get(int(ans[0]))


    def knc(self):
        last = 0 

        for _ in range(2,50):
            knn = self.knn.fit(self.xtr,self.ytr)
            score = knn.score(self.xts,self.yts)
            if score > last:
                self.knn = knn
                last = score

        for key in self.predictionValue:
            ans = self.knn.predict([self.predictionValue.get(key)])
            print(ans[0])
            print(f"\nPrediciton for {key} : {self.predictDict.get(int(ans[0]))}")      
    
        return self.predictDict.get(int(ans[0]))
        
pred = predictionInstance()
pred.assignInput()